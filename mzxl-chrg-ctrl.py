#!/usr/bin/env python

import logging
import os
import time

import libmzxl as i2cf


def charging_boolean2text(charging):
    return 'charging' if charging else 'discharging'


# setup charging process
i2cf.setup_gpio()
i2cf.gpio_charger_on()

# check initial power connection
i2cf.adc_switch_on()
inputType = i2cf.vbus_stat()
# VBUS Detection Status
# 000 - No Input
# 001 - USB Host SDP
# 010 - USB CDP (1.5 A)
# 011 - Adapter (3.0A)
# 100 - POORSRC detected 7 consecutive times
# 101 - Unknown Adapter (500 mA)
# 110 - Non-standard Adapter (1 A/2 A/2.1 A/2.4 A)
# 111 - OTG
chargingProgress = inputType != '000'

logging.basicConfig(filename='/tmp/mzxl-chrg-ctrl.log', level=logging.INFO, format='%(message)s')

while True:
    i2cf.adc_switch_on()
    ichg = i2cf.ichg_adc()
    chargerStatus = i2cf.charger_status()
    # logging.info("VBAT ADC: %s mV" % i2cf.vbat_adc())
    vbattery = i2cf.vbat_adc_clean(chargingProgress)

    logging.info("******************")
    logging.info("Time: %s" % i2cf.current_timestamp())
    logging.info("Charger state: (%s) %s" % (chargerStatus, charging_boolean2text(chargerStatus != '000')))
    logging.info("VBAT ADC: %s mV" % vbattery)
    logging.info("ICHG ADC: %s mA" % ichg)

    # Battery precharge to fast-charge threshold: 6000 mV
    # PoweroffLimit an LowLimit must be greater than this threshold (6000 mV)
    # PoweroffLimit must be lower than LowLimit
    vbatteryHighLimit = 7900
    vbatteryLowLimit = 6200
    vbatteryPoweroffLimit = 6100

    assert vbatteryPoweroffLimit < vbatteryLowLimit, "PoweroffLimit must be lower than LowLimit"

    if vbattery > vbatteryHighLimit and chargingProgress:
        i2cf.gpio_charger_off()
        chargingProgress = False

    if vbattery < vbatteryLowLimit and not chargingProgress:
        i2cf.gpio_charger_on()
        chargingProgress = True

    # ignore chargingProcess
    if vbattery < vbatteryPoweroffLimit:
        logging.info("Battery LOW!!!")
        i2cf.adc_switch_on()
        # if no charger -> shutdown
        if i2cf.vbus_stat() == '000':
            # power off signal
            logging.info("Shutdown started")
            # Scheduled shutdown
            os.system("sudo shutdown")
            exit()  # program termination

    logging.info("Charger NEW state: %s" % charging_boolean2text(chargingProgress))
    logging.info("******************\n")

    # 2 minutes
    time.sleep(2*60)
