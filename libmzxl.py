#!/usr/bin/env python

import datetime
import time

import RPi.GPIO as GPIO
import smbus

I2C_ADDR = 0x6b
CHRG_ONOFF_GPIO_PORT = 16
bus = smbus.SMBus(1)


def decimal_to_binary(num):
    return bin(num).replace("0b", "")


def read_byte_data(reg):
    return decimal_to_binary(bus.read_byte_data(I2C_ADDR, reg))


def read_dualregister_data(reg1, reg0):
    return int((read_byte_data(reg1) + read_byte_data(reg0).rjust(8, '0')), 2)


# ================================================

def charger_status():
    # last 3 bits needed
    return read_byte_data(0x0B)[-3:]
    # return read_byte_data(0x0B).rjust(8, '0')


def ibus_adc():
    return read_dualregister_data(0x17, 0x18)


def ichg_adc():
    return read_dualregister_data(0x19, 0x1A)


def vbat_adc():
    return read_dualregister_data(0x1D, 0x1E)


def vbat_adc_clean(charging_is_enabled):
    vbattery = vbat_adc()
    if charging_is_enabled:
        disable_charging()
        # wait for voltage relapse
        time.sleep(3)
        adc_switch_on()
        vbattery = vbat_adc()
        enable_charging()
    return vbattery


def vbus_adc():
    return read_dualregister_data(0x1B, 0x1C)


def vsys_adc():
    return read_dualregister_data(0x1F, 0x20)


def adc_switch_on():
    # ADC_EN = 1		* Enable ADC
    # ADC_RATE = 1/0	* Onse-shot / Continuous conversion
    # ADC_SAMPLE = 00 	* 15 bit effective resolution
    # 0000 - reserved
    bus.write_byte_data(I2C_ADDR, 0x15, int('11000000', 2))
    time.sleep(1)  # wait for on-shot conversion termination


def adc_switch_off():
    # ADC_EN = 0		* Disable ADC
    # ADC_RATE = 0		* Continuous conversion
    # ADC_SAMPLE = 11 	* 12 bit effective resolution
    # 0000 - reserved
    bus.write_byte_data(I2C_ADDR, 0x15, int('00110000', 2))


def adc_status():
    return read_byte_data(0x15).rjust(8, '0')


def charger_control1_status():
    return read_byte_data(0x05).rjust(8, '0')


def charger_control2_status():
    return read_byte_data(0x06).rjust(8, '0')


def enable_charging():
    bus.write_byte_data(I2C_ADDR, 0x06, int('01111101', 2))


def disable_charging():
    bus.write_byte_data(I2C_ADDR, 0x06, int('01110101', 2))


def vbus_stat():
    return read_byte_data(0x0C).rjust(8, '0')[1:4]


def ico_ilim():
    # return int(read_byte_data(0x0A).rjust(8, '0')[3:], 2)
    return 100 * int(read_byte_data(0x0A).rjust(8, '0')[3:], 2)


def current_timestamp():
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')


# ================ GPIO functions ================

def setup_gpio():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(CHRG_ONOFF_GPIO_PORT, GPIO.OUT)


def gpio_charger_off():
    GPIO.output(CHRG_ONOFF_GPIO_PORT, GPIO.HIGH)


def gpio_charger_on():
    GPIO.output(CHRG_ONOFF_GPIO_PORT, GPIO.LOW)
