#!/usr/bin/env python

import sys

import libmzxl as i2cf

# available commands
AC = ["on", "off"]

i2cf.setup_gpio()

if len(sys.argv) == 2 and sys.argv[1] in AC:
    if sys.argv[1] == "on":
        i2cf.gpio_charger_on()
        print "MZXL Charger: ON"
    else:
        i2cf.gpio_charger_off()
        print "MZXL Charger: OFF"
else:
    print "Usage: python mzxl-chrg-switch < on | off >"
