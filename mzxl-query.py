#!/usr/bin/env python

import libmzxl as i2cf

# import RPi.GPIO as GPIO

# enter to host mode
i2cf.adc_switch_on()

separator = "**********************************"
print(separator)
print("Time: %s" % i2cf.current_timestamp())
print(separator)
print("Charge Status bits:")
print("000 - Not Charging")
print("001 - Trickle Charge (VBAT < VBAT_SHORT)")
print("010 - Pre-charge (VBAT_UVLO_RISING < VBAT < VBAT_LOWV)")
print("011 - Fast-charge (CC mode)")
print("100 - Taper Charge (CV mode)")
print("101 - Top-off Timer Charging")
print("110 - Charge Termination Done")
print("111 - Reserved")
print("Charger Status: %s" % i2cf.charger_status())
print(separator)
print("VBUS Detection Status")
print("000 - No Input")
print("001 - USB Host SDP")
print("010 - USB CDP (1.5 A)")
print("011 - Adapter (3.0A)")
print("100 - POORSRC detected 7 consecutive times")
print("101 - Unknown Adapter (500 mA)")
print("110 - Non-standard Adapter (1 A/2 A/2.1 A/2.4 A)")
print("111 - OTG")
print("VBUS_STAT: %s" % i2cf.vbus_stat())
print(separator)
print("ICO_ILIM: %s mA" % i2cf.ico_ilim())
print(separator)
# default: 01111101
# i2cf.enable_charging()
# i2cf.disable_charging()
print("Charger control1 status: %s" % i2cf.charger_control1_status())
print("Charger control2 status: %s" % i2cf.charger_control2_status())
print(separator)

print("IBUS ADC: %s mA" % i2cf.ibus_adc())
print("ICHG ADC: %s mA" % i2cf.ichg_adc())

print("VBUS ADC: %s mV" % i2cf.vbus_adc())
print("VSYS ADC: %s mV" % i2cf.vsys_adc())
print("VBAT ADC: %s mV" % i2cf.vbat_adc())

print(separator)
print("ADC Status: %s" % i2cf.adc_status())
print(separator)

# adc_switch_off()
